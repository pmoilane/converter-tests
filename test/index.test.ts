import converter from "../src/index";

describe("division", () => {
    it("Returns oz with parameters ,  and ", () => {
        expect(converter("pint", "l", 1)).toBe(0.57);
    });
    it("Returns  with parameters ,  and ", () => {
        expect(converter("oz", "dl", 6)).toBe(1.7);
    });
    it("Returns  with parameters ,  and ", () => {
        expect(converter("cup", "oz", 15)).toBe(150);
    });
    it("Returns  with parameters ,  and ", () => {
        expect(converter("l", "cup", 10)).toBe(35.2);
    });
    it("Returns  with parameters ,  and ", () => {
        expect(converter("cl", "dl", 3)).toBe("invalid unit(s), use dl, l, oz, cup or pint");
    });
});