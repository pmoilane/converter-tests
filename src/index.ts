const volume = Number(process.argv[2]);
const unit1 = process.argv[3];
const unit2 = process.argv[4];
console.log(converter(unit1, unit2, volume));

// uses UK units, volume get first converted from unit1 to liters and then to unit specified by unit2
export default function converter(unit1: string, unit2: string, volume: number) {
    const allowedUnits = ["dl", "l", "oz", "cup", "pint"];
    if (allowedUnits.includes(unit1) && allowedUnits.includes(unit2)) {
        if (unit1 === "dl") {
            volume = volume * 0.1;
        } else if (unit1 === "oz") {
            volume = volume * 0.0284130625;
        } else if (unit1 === "cup") {
            volume = volume * 0.284130625;
        } else if (unit1 === "pint") {
            volume = volume * 0.56826125;
        }
        if (unit2 === "dl") {
            volume = volume * 10;
        } else if (unit2 === "oz") {
            volume = volume * 35.195079728;
        } else if (unit2 === "cup") {
            volume = volume * 3.5195079728;
        } else if (unit2 === "pint") {
            volume = volume * 1.7597539864;
        }
        return Math.round(volume * 100) / 100;
    } else {
        return "invalid unit(s), use dl, l, oz, cup or pint";
    }
}